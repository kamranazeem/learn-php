<?
require_once 'includes/security.php';
require_once 'includes/dbconnection.php';
if(!isset($_POST['chkid']) || empty($_POST['chkid'])){
	header('location:employeelist.php?msg=Please select employees');
	exit;
}
?>
<html>
	<head>
		<link rel="stylesheet" href="css/styles.css">
	</head>
	<body>
		<div id="page">
			<div id="header">
				<?php require_once 'includes/header.php'; ?>
			</div>
			<div id="content">
				<div id="leftpanel">
					<?php require_once 'includes/leftpanel.php'; ?>
				</div>
				<div id="body">
					<div>
						<?
						$query = "delete from employees where id in (".implode(',',$_POST['chkid']).")";
						$check = mysqli_query($dbconnection, $query);
						if($check){
						?>
							<div>
								Record(s) deleted successfully
							</div>
						<?
						}
						else{
						?>
							<div>
								Record(s) not deleted
							</div>
						<?
						}
						?>						
					</div>
				</div>
			</div>
			<div id="footer">
				<?php require_once 'includes/footer.php'; ?>
			</div>
		</div>
	</body>
</html>
