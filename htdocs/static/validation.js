function validate(objfrm){
	if(objfrm.txtname.value == "")
	{
		alert("Please enter name");
		objfrm.txtname.focus();
		return false;
	}
	else if(objfrm.txtaddress.value == "")
	{
		alert("Please enter address");
		objfrm.txtaddress.focus();
		return false;
	}
	else if(objfrm.txtemail && objfrm.txtemail.value == "")
	{
		alert("Please enter email");
		objfrm.txtemail.focus();
		return false;
	}
	else if(objfrm.txtpassword && objfrm.txtpassword.value == "")
	{
		alert("Please enter password");
		objfrm.txtpassword.focus();
		return false;
	}
	return true;
}

function validateLogin(objfrm)
{
	if(objfrm.txtusername.value == "")
	{
		alert("Please enter username");
		objfrm.txtusername.focus();
		return false;
	}
	else if(objfrm.txtpassword.value == "")
	{
		alert("Please enter password");
		objfrm.txtpassword.focus();
		return false;
	}
	return true;
}
//This function will call jquery validation on submit button click to validate salary field
//following function will attach event handler to button click of btnsubmit button
// This does not require explicit call in form-submit or button-click, as done in java script.
$( document ).ready(function() {
    $("#btnsubmit").click(function(){
		if($("#txtsalary").val() =="")
		{
			alert("Please enter salary");
			return false;
		}
		return true;
    });
});
