<?
session_start();
?>
<html>
	<head>
		<link rel="stylesheet" href="css/styles.css">
	</head>
	<body>
		<div id="page">
			<div id="header">
				<?php require_once 'includes/header.php'; ?>
			</div>
			<div id="content">
				<div id="leftpanel">
					<?php require_once 'includes/leftpanel.php'; ?>
				</div>
				<div id="body">
					Body
				</div>
			</div>
			<div id="footer">
				<?php require_once 'includes/footer.php'; ?>
			</div>
		</div>
	</body>
</html>
