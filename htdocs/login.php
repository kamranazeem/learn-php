<html>
	<head>
		<script src="static/jquery-3.4.1.js"></script>
		<script src="static/validation.js"></script>
		<link rel="stylesheet" href="css/styles.css">
	</head>
	<body>
		<div id="page">
			<div id="header">
				<?php require_once 'includes/header.php'; ?>
			</div>
			<div id="content">
				<div id="leftpanel">
					<?php require_once 'includes/leftpanel.php'; ?>
				</div>
				<div id="body">
					<div>
						<div class="pageheading">Login</div>
						<div class="msg"><? isset($_GET['msg']) ? print $_GET['msg'] : "" ?></div>
						<div class="msg">*(Required)</div>
						<form method="post" action="loginaction.php" id="frmaddemployee" onsubmit="return validateLogin(this);">
							<div>
								<div class="formrow">
									<div class="label">Username*</div>
									<div class="element"><input type="text" name="txtusername" id="txtusername" value=""></div>
								</div>
								<div class="formrow">
									<div class="label">Password*</div>
									<div class="element"><input type="password" name="txtpassword" id="txtpassword" value=""></div>
								</div>
								<div class="formrow">
									<div class="label"></div>
									<div class="element"><input type="submit" name="btnsubmit" id="btnsubmit" value="Login"></div>
								</div>
							</div>					
						</form>	
					</div>
				</div>
			</div>
			<div id="footer">
				<?php require_once 'includes/footer.php'; ?>
			</div>
		</div>
	</body>
</html>
