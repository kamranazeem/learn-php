<?
require_once 'includes/dbconnection.php';
if(isset($_POST['btnsubmit']))
{
	if($_POST['txtname'] == ""){
		header('location:addemployee.php?msg=Name is empty');
		exit;
	}
	elseif($_POST['txtaddress'] == ""){
		header('location:addemployee.php?msg=Address is empty');
		exit;
	}
	elseif($_POST['txtsalary'] == ""){
		header('location:addemployee.php?msg=Salary is empty');
		exit;
	}

	$id = filter_var($_POST['hdnid'], FILTER_VALIDATE_INT);
	$name = filter_var($_POST['txtname'], FILTER_SANITIZE_STRING);
	$address = filter_var($_POST['txtaddress'], FILTER_SANITIZE_STRING);
	$salary = filter_var($_POST['txtsalary'], FILTER_VALIDATE_INT);
	$query = "update employees set name = '".$name."'".
			", address = '".$address."'".
			", salary = '".$salary."'".
			" where id = ".$id;

	$check = mysqli_query($dbconnection, $query);
	if($check){
		header("location:employeelist.php?msg=Employee updated successfully");
		exit;
	}
	else{
		header("location:employeelist.php?msg=Employee not updated");
		exit;
	}
}
else{
	header("location:employeelist.php?msg=Employee not updated");
	exit;
}
?>						
