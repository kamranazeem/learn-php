<?
require_once 'includes/security.php';
require_once 'includes/dbconnection.php';
if(!isset($_GET['id'])){
	header('location:employeelist.php?msg=Please select employee for edit');
	exit;
}
$query = "select * from employees where id = ".$_GET['id'];
$rs = mysqli_query($dbconnection, $query);
if(mysqli_num_rows($rs) > 0){
	$row = mysqli_fetch_array($rs);
}
else{
	header('location:employeelist.php?msg=Invalid employee ID');
	exit;
}
?>
<html>
	<head>
		<script src="static/jquery-3.4.1.js"></script>
		<script src="static/validation.js"></script>
		<link rel="stylesheet" href="css/styles.css">
	</head>
	<body>
		<div id="page">
			<div id="header">
				<?php require_once 'includes/header.php'; ?>
			</div>
			<div id="content">
				<div id="leftpanel">
					<?php require_once 'includes/leftpanel.php'; ?>
				</div>
				<div id="body">
					<div>
						<div class="pageheading">Edit Employee</div>
						<div class="msg"><? isset($_GET['msg']) ? print $_GET['msg'] : "" ?></div>
						<div class="msg">*(Required)</div>
						<form method="post" action="editemployeeaction.php" id="frmaddemployee" onsubmit="return validate(this);">
							<div>
								<div class="formrow">
									<div class="label">Name*</div>
									<div class="element"><input type="text" name="txtname" id="txtname" value="<?=$row['name']?>"></div>
								</div>
								<div class="formrow">
									<div class="label">Address*</div>
									<div class="element"><textarea name="txtaddress" id="txtaddress" value=""><?=$row['address']?></textarea></div>
								</div>
								<div class="formrow">
									<div class="label">Salary*</div>
									<div class="element"><input type="text" name="txtsalary" id="txtsalary" value="<?=$row['salary']?>"></div>
								</div>
								<div class="formrow">
									<div class="label"></div>
									<div class="element">
										<input type="hidden" name="hdnid" value="<?=$_GET['id']?>">
										<input type="submit" name="btnsubmit" id="btnsubmit" value="Update"></div>
								</div>
							</div>					
						</form>	
					</div>
				</div>
			</div>
			<div id="footer">
				<?php require_once 'includes/footer.php'; ?>
			</div>
		</div>
	</body>
</html>
