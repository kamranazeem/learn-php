<?
require_once 'includes/dbconnection.php';
if(isset($_POST['btnsubmit']))
{
	if($_POST['txtname'] == ""){
		header('location:addemployee.php?msg=Name is empty');
		exit;
	}
	elseif($_POST['txtaddress'] == ""){
		header('location:addemployee.php?msg=Address is empty');
		exit;
	}
	elseif($_POST['txtsalary'] == ""){
		header('location:addemployee.php?msg=Salary is empty');
		exit;
	}
	elseif($_POST['txtemail'] == ""){
		header('location:addemployee.php?msg=Email is empty');
		exit;
	}
	elseif($_POST['txtpassword'] == ""){
		header('location:addemployee.php?msg=Password is empty');
		exit;
	}

	$name = filter_var($_POST['txtname'], FILTER_SANITIZE_STRING);
	$address = filter_var($_POST['txtaddress'], FILTER_SANITIZE_STRING);
	$salary = filter_var($_POST['txtsalary'], FILTER_VALIDATE_INT);
	$email = filter_var($_POST['txtemail'], FILTER_SANITIZE_STRING);
	$password = sha1(filter_var($_POST['txtpassword'], FILTER_SANITIZE_STRING));
	$picture = addslashes(file_get_contents($_FILES['flpicture']['tmp_name']));
	$query = "insert into employees(name, address, salary, username, password, picture) values(".
				"'".$name."','".$address."','".$salary."','".$email."','".$password."','".$picture."')";
	$check = mysqli_query($dbconnection, $query);
	if($check){
		header("location:employeelist.php?msg=Employee added successfully");
		exit;
	}
	else{
		header("location:employeelist.php?msg=Employee not added");
		exit;
	}
}
else{
	header("location:employeelist.php?msg=Employee not added");
	exit;
}
?>						
